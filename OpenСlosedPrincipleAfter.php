<?php

interface Communicative
{
    public function speak(): string;
}

class Dog implements Communicative
{
    public function speak(): string
    {
        return 'woooooooooof';
    }
}

class Cat implements Communicative
{
    public function speak(): string
    {
        return 'meeeeeoooooow';
    }
}

class Bird implements Communicative
{
    public function speak(): string
    {
        return 'chirp chirp';
    }
}

class Communication
{
    public function communicate(Communicative $animal): string
    {
        return $animal->speak();
    }
}