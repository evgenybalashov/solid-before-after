<?php

interface WorkInterface
{
    public function work(): string;
}

interface SleepInterface
{
    public function sleep(): string;
}

class HumanWorker implements WorkInterface, SleepInterface
{
    public function work(): string
    {
        return 'works';
    }

    public function sleep(): string
    {
        return 'sleep';
    }
}

class RobotWorker implements WorkInterface
{
    public function work(): string
    {
        return 'works';
    }
}
