<?php

interface ConnectionInterface
{
    public function connect(): void;
}

class DbConnection implements ConnectionInterface
{

    public function connect(): void
    {
      //...
    }
}

class PasswordReminder
{
    /**
     * @var ConnectionInterface
     */

    private $dbConnection;

    public function __construct(ConnectionInterface $dbConnection)
    {
        $this->dbConnection = $dbConnection;
    }
}