<?php

interface ArticleRepositoryInterface
{
    public function getAll(): array;
}

class FileArticleRepository implements ArticleRepositoryInterface
{
    public function getAll(): array
    {
        // return through file system
        return [];
    }
}

class DbArticleRepository implements ArticleRepositoryInterface
{
    public function getAll(): array
    {
        return Article::all()->toArray();
    }
}