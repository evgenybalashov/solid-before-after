<?php


class Dog
{
    public function woof(): string
    {
        return 'woooooooooof';
    }
}
class Cat
{
    public function meow(): string
    {
        return 'meeeeeoooooow';
    }
}
class Bird
{
    public function chirp(): string
    {
        return 'chirp chirp';
    }
}

class Communication
{
    public function communicate($animal): string
    {
        return match (true) {
            $animal instanceof Dog => $animal->woof(),
            $animal instanceof Cat => $animal->meow(),
            $animal instanceof Bird => $animal->chirp(),
            default => throw new \InvalidArgumentException('Unknown animal'),
        };
    }
}

