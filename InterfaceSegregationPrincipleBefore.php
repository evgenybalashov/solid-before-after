<?php

interface WorkerInterface
{
    public function work(): string;

    public function sleep(): string;
}

class HumanWorker implements WorkerInterface
{
    public function work(): string
    {
        return 'works';
    }

    public function sleep(): string
    {
        return 'sleep';
    }
}

class RobotWorker implements WorkerInterface
{
    public function work(): string
    {
        return 'works';
    }

    public function sleep(): string
    {
        //NO NEED
        return 'sleep';
    }
}