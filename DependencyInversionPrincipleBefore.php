<?php


class DbConnection
{

    public function connect():void
    {
        //...
    }
}

class PasswordReminder
{
    /**
     * @var DbConnection
     */
    private $dbConnection;


    public function __construct(DbConnection $dbConnection)
    {
        $this->dbConnection = $dbConnection;
    }
}