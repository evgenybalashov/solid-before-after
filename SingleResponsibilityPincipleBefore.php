<?php

//1. Какой это принцип?
//2. Чтобы вы поменяли?

class Article
{
    private string $title;
    private string $body;
    private Author $author;
    private \DateTime $date;

    // ..

    public function getData(): array
    {
        return [
            'title' => $this->title,
            'body' => $this->body,
            'author' => $this->author->fullName(),
            'timestamp' => $this->date->getTimestamp(),
        ];
    }

    public function formatJson(): string
    {
        return json_encode($this->getData());
    }

    public function formatHtml(): string
    {
        return `<article>
                    ....
                </article>`;
    }
}