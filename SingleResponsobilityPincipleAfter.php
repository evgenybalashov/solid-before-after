<?php

interface FormatArticle
{
    public function format(Article $article);
}

class JsonArticleFormat implements FormatArticle
{
    public function format(Article $article)
    {
        return json_encode($article->getData());
    }
}

class HtmlArticleFormat implements FormatArticle
{
    public function format(Article $article)
    {
        return `<article>
                    ...
                </article>`;
    }
}